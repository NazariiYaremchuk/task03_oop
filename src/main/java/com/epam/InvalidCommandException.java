package com.epam;

/**
 * Package for my developing.
 *
 * @author Nazarii Yaremchuk.
 * @since 11-11-2019.
 */


class InvalidCommandException extends Throwable {
    /**
     * Value message of type String.
     */
    private String message;

    /**
     *
     * @param s value of message
     */
    InvalidCommandException(final String s) {
        message = s;
    }

    /**
     * method prints our message.
     */
    public void printMessage() {
        System.out.println(message);
    }
}
